/*
    Proba-choreo
    Movement sequence generator based in occurrence probabilities
    Copyright (C) 2016 José Vega-Cebrián (Sejo) jmvc[a]escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var titlediv;
var maindiv;
var inputsdiv;
var navbar;
var prevButton, nextButton;

var state = 0; // Instructions, set actions, set probabilities, get sequence
var NUM_STATES = 4;

var DEFAULTVALUE = 50;

var actions = [];

var roulette = [];

function setup() {
	titlediv = createDiv('<h1>proba-choreo</h1>');
	titlediv.parent('probachoreo');
	titlediv.id('titlediv');

	maindiv = createDiv('');
	maindiv.parent('probachoreo');

	setMainDiv();

	navbar = createDiv('');
	navbar.id('navbar');
	navbar.parent('probachoreo');

	setNavbar();
	updateNavbar();

	for(var i=0; i<4; i++){
		actions.push(new action('',DEFAULTVALUE));
	}

}

function draw() {
  
}

function setMainDiv(){
	maindiv.html('');
	switch(state){
		case 0: // instructions;
			var spa = `
					proba-choreo es un generador de secuencias de movimiento
					basado en asignar probabilidades de aparición
					a las acciones coreográficas.
					<br/><br/>
					1. Escribe las acciones de la coreografía<br/>
					2. Determina sus probabilidades de aparición<br/>
					3. Escoge el número de acciones por secuencia<br/> 
					4. Genera secuencias
					`;
			var eng = `
					proba-choreo is a movement sequence generator
					based in occurrence probabilities that are set to
					the choreographic actions.
					<br/><br/>
					1. Write the actions of the choreography<br/>
					2. Set their occurrence probabilities<br/>
					3. Choose the number of actions per sequence<br/>
					4. Generate sequences
					`;
			setPs(spa,eng,maindiv);
			break;
		case 1: // Actions
			var spa = `
					Escribe las acciones de la coreografía,
					una en cada línea. Puedes agregar más o borrarlas
					con los botones correspondientes.
					`;
			var eng = `
					Write the actions of the choreography,
					one per input box.
					You can add more or delete them with the 
					corresponding buttons
					`;
			setPs(spa,eng,maindiv);

			inputsdiv = createDiv('');
			inputsdiv.parent(maindiv);

			for(var i = 0; i < actions.length; i++){
				createInputRow(i);
			}

			var p = createP('');
			p.parent(maindiv);

			button = createButton('agregar/add');
			button.parent(maindiv);
			button.mousePressed(addInput);
		break;

		case 2: // Probabilities
			cleanupActions();
			var spa = `
					Determina la probabilidad de aparición para cada acción.
					Toma en cuenta que el porcentaje es calculado respecto
					a la suma de todos los sliders.
					`;
			var eng = `
					Set the occurrence probability for each action.
					The percentage is calculated with respect to the sum
					of all the sliders.
					`;
			setPs(spa,eng,maindiv);

			for(var i = 0; i < actions.length; i++){
				createSliderRow(i);
			}

		break;

		case 3: // Generate!
			createRoulette();

			var spa = `
					Escoge el número de acciones en la secuencia de movimiento
					y presiona el botón para generarla tantas veces como 
					quieras.
					`;
			var eng = `
					Choose the number of actions in the movement sequence
					and press the button to generate it as many times
					as you want.
					`;
			setPs(spa,eng,maindiv);


			var p = createP('');
			p.parent(maindiv);

			var slider = createSlider(1,100,10);
			slider.parent(p);
			slider.id("numSlider");
			slider.input(updateNumSlider);

			var span = createSpan(" "+slider.value());
			span.id("spanNumSlider");
			span.parent(p);


			var button = createButton("generar/generate");
			button.parent(maindiv);
			button.mousePressed(generate);

			var results = createDiv('');
			results.id("resultsDiv");
			results.parent(maindiv);

		break;



	}

}

function createSliderRow(i){
	var p = createP('');
	p.parent(maindiv);

	// Create slider
	var slider = createSlider(0,100,actions[i].value);
	slider.parent(p);
	slider.class(i);
	slider.input(updateActionValueWithInput);

	// Create and assign texxt
	var text = getSliderText(i);
	var s = createSpan(text);
	s.id("span"+i);
	s.parent(p);


}

function generate(){
	var slider = select("#numSlider");
	var sequencesize = slider.value();
	var index;

	var resultsdiv = select("#resultsDiv");
	resultsdiv.html('');

	for(var i=1; i<=sequencesize; i++){
		index = roulette[floor(random(roulette.length))];	
		var p = createP(i+". "+actions[index].name);
		p.parent(resultsdiv);
	}


}

// Fill an array with the indexes of actions, according to the slider values
function createRoulette(){
	roulette = [];

	for(var i = 0; i < actions.length; i++){
		for( var j = 0; j < actions[i].value; j++){
			roulette.push(i);
		}
	}

	print(roulette.length + " " + sumAll());


}

// Callback function for the num slider
function updateNumSlider(){
	var span = select("#spanNumSlider");
	var slider = select("#numSlider");
	span.html(" "+slider.value());

}

// Update all texts
function updateSliderTexts(){
	for(var i = 0; i < actions.length; i++){
		var s = select('#span'+i);
		s.html(getSliderText(i));
	}
}

// Construct the formatted text for the sliders
function getSliderText(i){
	var text = " "+actions[i].name+": "+percentage(actions[i].value)+"%";
	return text;
}

// Get the corresponding percentage of num respect sumAll
function percentage(num){
	return round(100*num/sumAll());
}

// Get the sum of the values of all active actions
function sumAll(){
	var sum = 0;
	for(var i = 0; i < actions.length; i++){
		if(actions[i].active){
			sum += actions[i].value;
		}
	}
	return sum;
}

// Remove the actions with active == false
function cleanupActions(){
	for(var i = 0; i < actions.length; i++){
		if(!actions[i].active){
			actions.splice(i,1);
		}
	}

}

// Update the object name with the text in the input
function updateActionNameWithInput(){
	var c = this.class();
	actions[c].name = this.value();

}

// Update the object value with the value of the slider
function updateActionValueWithInput(){
	var c = this.class();
	actions[c].value = this.value();

	updateSliderTexts();
}

// Function to generate an input with a delete button, along with their callback functions
function createInputRow(i){
	var input = createInput(actions[i].name);
	input.parent(inputsdiv);
	input.class(i);
	input.input(updateActionNameWithInput);

	var button = createButton('borrar/delete');
	button.parent(inputsdiv);
	button.class(i);
	button.mousePressed(deleteInput);
	var p = createP('');
	p.parent(inputsdiv);
}

// Callback function for the add button
function addInput(){
	actions.push(new action('',DEFAULTVALUE));
	createInputRow(actions.length-1);
}

// Callback function for the delete button
function deleteInput(){
	var c = this.class();
	var elements = selectAll('.'+c);

	actions[c].active = false;

	for(var i = 0; i < elements.length; i++){
		elements[i].remove();
	}
}

// Set the spanish and english Ps for a given parent
function setPs(spaString, engString,par){
	var p = createP(spaString);
	p.class('normal');
	p.parent(par);
	p = createP(engString);
	p.class('bold');
	p.parent(par);

}

function nextState(){
	if(state<NUM_STATES-1){
		state++;
	}
	setMainDiv();
	updateNavbar();
}

function prevState(){
	if(state>0){
		state--;
	}
	setMainDiv();
	updateNavbar();
}

function setNavbar(){
	prevButton = createButton("anterior/prev");
	prevButton.mousePressed(prevState);
	prevButton.parent(navbar);


	nextButton = createButton("siguiente/next");
	nextButton.mousePressed(nextState);
	nextButton.parent(navbar);
}

function updateNavbar(){
	navbar.html('');

	if(state>0){
		prevButton = createButton("anterior/prev");
		prevButton.mousePressed(prevState);
		prevButton.parent(navbar);

	}

	if(state<3){
		nextButton = createButton("siguiente/next");
		nextButton.mousePressed(nextState);
		nextButton.parent(navbar);
	}


	/*

	if(state==0){
		prevButton.attribute('disabled','');
	}
	else{
		prevButton.attribute('disabled',null);
	}

	if(state==3){
		nextButton.attribute('disabled','true');
	}
	else{
		nextButton.attribute('disabled');
	}
	*/
}


function action(name, value){
	this.name = name;
	this.value = value;
	this.active = true;
}
