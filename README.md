# Proba-choreo
proba-choreo is a movement sequence generator based in occurrence probabilities that are set to the choreographic actions. 

![proba-choreo Screenshot](http://escenaconsejo.org/wp-content/uploads/2016/07/Selection_126.png)

The generator is located in [http://escenaconsejo.org/choreogen/proba-choreo/](http://escenaconsejo.org/choreogen/proba-choreo/)

More information can be found [here](http://escenaconsejo.org/trayectoria/coreografias/generadores-coreograficos/proba-choreo/)

Built with [p5.js](http://p5js.org)
